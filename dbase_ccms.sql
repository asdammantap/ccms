-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2015 at 06:30 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbase_ccms`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_anggota`
--

CREATE TABLE IF NOT EXISTS `t_anggota` (
  `noktp` char(50) NOT NULL DEFAULT '',
  `nama` varchar(100) DEFAULT NULL,
  `alamat` text,
  `notelp` char(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `jeniskel` varchar(12) DEFAULT NULL,
  `tmptlahir` varchar(30) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `pekerjaan` varchar(30) DEFAULT NULL,
  `username` varchar(20) DEFAULT '',
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_anggota`
--

INSERT INTO `t_anggota` (`noktp`, `nama`, `alamat`, `notelp`, `email`, `jeniskel`, `tmptlahir`, `tgllahir`, `pekerjaan`, `username`, `password`) VALUES
('131834194189', 'Jabrix', 'Serdang', '089318931', 'jabrix@min.com', 'laki-laki', 'Serang', '1990-10-10', 'Pelajar', 'jabrix', 'jabrix'),
('3604050501101990', 'Nurul Huda', 'Seraang', '087781784741', 'asdal@yahoo.co.id', 'perempuan', 'Batam', '1998-09-09', 'Pelajar', 'nurul', 'nurul'),
('3604050901102014', 'Zainab', 'Cilegon', '0894892', 'asdam@ja.com', 'perempuan', 'bata', '1995-09-09', 'swasta', 'zainab', 'zainab'),
('3604050901102015', 'M.A. Saddam', 'Serdang', '08783018981', 'asdamgrimson@gmail.com', 'laki-laki', 'Batam', '1990-11-09', 'Pengajar', 'asdam', '1404770ac0138a5c3b6fe09aaf738cdb'),
('42040204020', 'safaah', 'djakdkajs', '0842948928', 'safaah@gmail.com', 'perempuan', 'batam', '1970-10-10', 'swasta', 'safaah', 'ed2c5b39b85f1df21d32535b3f7bc6cc'),
('47357583768', 'jdkjak', 'jkjkj', '031893189', 'jjk@gmail.com', 'laki-laki', 'batam', '1998-10-10', 'swasta', 'asiaysua', 'aoodaoi'),
('664387909', 'Asmunipo', 'adhajhj', '081983189141', 'asmunidin@gmail.com', 'laki-laki', 'serang', '1970-10-10', 'swasta', 'asmu', '35b4457fe9c4ad05468ad5c1fa252a60'),
('990492022', 'Supinah', 'kramatwatu', '08397328', 'supin@gmail.com', 'laki-laki', 'serang', '1990-04-01', 'swasa', 'asiyah', '367f7790e1588cc53aed634c1e9df2d7');

-- --------------------------------------------------------

--
-- Table structure for table `t_bank`
--

CREATE TABLE IF NOT EXISTS `t_bank` (
  `id_bank` char(3) NOT NULL DEFAULT '',
  `nama_bank` varchar(30) DEFAULT NULL,
  `cabang` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_bank`
--

INSERT INTO `t_bank` (`id_bank`, `nama_bank`, `cabang`) VALUES
('B01', 'BCA', 'Cilegon'),
('B02', 'BNI', 'Cilegon'),
('B03', 'Bank Mandiri', 'Serang'),
('B04', 'Bank BRI', 'Pandeglang'),
('B05', 'Bank Syariah Mandiri', 'Serdang'),
('B06', 'Bank Jabar Banten', 'Cilegon');

-- --------------------------------------------------------

--
-- Table structure for table `t_jeniskk`
--

CREATE TABLE IF NOT EXISTS `t_jeniskk` (
  `id_cardtype` char(1) NOT NULL DEFAULT '',
  `cardtype` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jeniskk`
--

INSERT INTO `t_jeniskk` (`id_cardtype`, `cardtype`) VALUES
('c', 'Platinum'),
('M', 'Gold'),
('V', 'Visa');

-- --------------------------------------------------------

--
-- Table structure for table `t_kk`
--

CREATE TABLE IF NOT EXISTS `t_kk` (
  `noktp` char(50) NOT NULL DEFAULT '',
  `nokk` char(30) NOT NULL DEFAULT '',
  `id_bank` char(3) NOT NULL DEFAULT '',
  `id_cardtype` char(1) NOT NULL DEFAULT '',
  `expiredate` date DEFAULT NULL,
  `limit` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kk`
--

INSERT INTO `t_kk` (`noktp`, `nokk`, `id_bank`, `id_cardtype`, `expiredate`, `limit`) VALUES
('3604050901102015', '098765412561', 'B01', 'V', '2015-10-10', 3000000),
('990492022', '94029042424', 'B06', 'c', '2017-10-10', 500000000);

-- --------------------------------------------------------

--
-- Table structure for table `t_pengguna`
--

CREATE TABLE IF NOT EXISTS `t_pengguna` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(50) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pengguna`
--

INSERT INTO `t_pengguna` (`username`, `password`, `level`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
('asiaysua', 'aoodaoi', 'member'),
('asiyah', 'asiyah', 'member'),
('asmu', '35b4457fe9c4ad05468ad5c1fa252a60', 'member'),
('jabrix', 'jabrix', 'member'),
('komar', '9a362a91b4de4a313bed73647479200c', 'member'),
('safaah', 'ed2c5b39b85f1df21d32535b3f7bc6cc', 'member'),
('sjak', 'ajkk', 'member'),
('udin', '6bec9c852847242e384a4d5ac0962ba0', 'member'),
('user', 'user', 'member'),
('zainab', 'zainab', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `t_transaksikk`
--

CREATE TABLE IF NOT EXISTS `t_transaksikk` (
  `no_trans` char(10) NOT NULL DEFAULT '',
  `tgl_trans` date NOT NULL DEFAULT '0000-00-00',
  `noktp` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_transaksikk`
--

INSERT INTO `t_transaksikk` (`no_trans`, `tgl_trans`, `noktp`) VALUES
('T0001', '0000-00-00', '990492022');

-- --------------------------------------------------------

--
-- Table structure for table `t_transaksikkdet`
--

CREATE TABLE IF NOT EXISTS `t_transaksikkdet` (
  `no_trans` char(10) NOT NULL DEFAULT '',
  `nokk` char(30) NOT NULL DEFAULT '',
  `saldokeluar` bigint(25) DEFAULT NULL,
  `keperluan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kk`
--
CREATE TABLE IF NOT EXISTS `view_kk` (
`noktp` char(50)
,`nokk` char(30)
,`nama_bank` varchar(30)
,`cardtype` char(20)
,`batas` bigint(20)
,`expiredate` date
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_limitkk`
--
CREATE TABLE IF NOT EXISTS `view_limitkk` (
`noktp` char(50)
,`nokk` char(30)
,`nama_bank` varchar(30)
,`cardtype` char(20)
,`batas` bigint(20)
,`expiredate` date
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pengguna`
--
CREATE TABLE IF NOT EXISTS `view_pengguna` (
`noktp` char(50)
,`nama` varchar(100)
,`username` varchar(20)
,`pass` varchar(50)
,`level` varchar(20)
);
-- --------------------------------------------------------

--
-- Structure for view `view_kk`
--
DROP TABLE IF EXISTS `view_kk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kk` AS select `tkk`.`noktp` AS `noktp`,`tkk`.`nokk` AS `nokk`,`tb`.`nama_bank` AS `nama_bank`,`tj`.`cardtype` AS `cardtype`,`tkk`.`limit` AS `batas`,`tkk`.`expiredate` AS `expiredate` from ((`t_kk` `tkk` join `t_bank` `tb`) join `t_jeniskk` `tj`) where ((`tkk`.`id_bank` = `tb`.`id_bank`) and (`tkk`.`id_cardtype` = `tj`.`id_cardtype`));

-- --------------------------------------------------------

--
-- Structure for view `view_limitkk`
--
DROP TABLE IF EXISTS `view_limitkk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_limitkk` AS select `view_kk`.`noktp` AS `noktp`,`view_kk`.`nokk` AS `nokk`,`view_kk`.`nama_bank` AS `nama_bank`,`view_kk`.`cardtype` AS `cardtype`,`view_kk`.`batas` AS `batas`,`view_kk`.`expiredate` AS `expiredate` from `view_kk` where (`view_kk`.`batas` <= 100000);

-- --------------------------------------------------------

--
-- Structure for view `view_pengguna`
--
DROP TABLE IF EXISTS `view_pengguna`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pengguna` AS select `ta`.`noktp` AS `noktp`,`ta`.`nama` AS `nama`,`ta`.`username` AS `username`,`ta`.`password` AS `pass`,`tp`.`level` AS `level` from (`t_anggota` `ta` join `t_pengguna` `tp`) where (`ta`.`username` = `tp`.`username`);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_anggota`
--
ALTER TABLE `t_anggota`
 ADD PRIMARY KEY (`noktp`), ADD KEY `username` (`username`);

--
-- Indexes for table `t_bank`
--
ALTER TABLE `t_bank`
 ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `t_jeniskk`
--
ALTER TABLE `t_jeniskk`
 ADD PRIMARY KEY (`id_cardtype`);

--
-- Indexes for table `t_kk`
--
ALTER TABLE `t_kk`
 ADD PRIMARY KEY (`nokk`,`noktp`), ADD KEY `noktp` (`noktp`), ADD KEY `id_bank` (`id_bank`), ADD KEY `id_cardtype` (`id_cardtype`);

--
-- Indexes for table `t_pengguna`
--
ALTER TABLE `t_pengguna`
 ADD PRIMARY KEY (`username`);

--
-- Indexes for table `t_transaksikk`
--
ALTER TABLE `t_transaksikk`
 ADD PRIMARY KEY (`no_trans`);

--
-- Indexes for table `t_transaksikkdet`
--
ALTER TABLE `t_transaksikkdet`
 ADD PRIMARY KEY (`no_trans`,`nokk`), ADD KEY `nokk` (`nokk`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_kk`
--
ALTER TABLE `t_kk`
ADD CONSTRAINT `t_kk_ibfk_1` FOREIGN KEY (`noktp`) REFERENCES `t_anggota` (`noktp`),
ADD CONSTRAINT `t_kk_ibfk_2` FOREIGN KEY (`id_bank`) REFERENCES `t_bank` (`id_bank`),
ADD CONSTRAINT `t_kk_ibfk_3` FOREIGN KEY (`id_cardtype`) REFERENCES `t_jeniskk` (`id_cardtype`);

--
-- Constraints for table `t_transaksikkdet`
--
ALTER TABLE `t_transaksikkdet`
ADD CONSTRAINT `t_transaksikkdet_ibfk_1` FOREIGN KEY (`no_trans`) REFERENCES `t_transaksikk` (`no_trans`),
ADD CONSTRAINT `t_transaksikkdet_ibfk_2` FOREIGN KEY (`nokk`) REFERENCES `t_kk` (`nokk`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
