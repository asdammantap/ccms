<?php 
	Class Model_user extends CI_Model {

		Function cek_user($data) {
			$query = $this->db->get_where('view_pengguna', $data);
			return $query;
		}

		/* public function get_all(){
		$data =  $this->db->query('select * from view_kk');
		 return $data;

		} */
		Function tampilData()
	{
		$query=$this->db->get('view_kk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
public function getkodeunik() { 
        $data = $this->db->query("SELECT MAX(RIGHT(no_trans,4)) AS noauto FROM t_transaksikk");
        $kd = ""; //kode awal
        if($data->num_rows()>0){ //jika data ada
            foreach($data->result() as $k){
                $tmp = ($k->noauto)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("s", $tmp); //kode ambil 4 karakter terakhir
            }
			}
			else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        $kar = "TRANS"; //karakter depan kodenya
        //gabungkan string dengan kode yang telah dibuat tadi
        
		return $kar.$kd;
   } 
Function tampilDataperid($id)
	{
		$this->db->where('nokk',$id); 
		$query=$this->db->get('view_kk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
Function tampilDataperidtrans($id)
	{
		$this->db->where('noktp',$id); 
		$query=$this->db->get('t_transaksikk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
Function tampilDatapribadi($id)
	{
		$this->db->where('noktp',$id); 
		$query=$this->db->get('t_anggota');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}

Function tampilDatawidget()
	{
		$this->db->group_by('cardtype');
		$query=$this->db->get('view_kk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
Function tampilDatawidgetlimit()
	{
		$this->db->group_by('batas');
		$query=$this->db->get('view_limitkk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}

function getbank()
 {
 $query = $this->db->query("SELECT * FROM t_bank");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['id_bank']] = $row['nama_bank'];
 }
 }
 return $data;
 }
 
 function getcardtype()
 {
 $query = $this->db->query("SELECT * FROM t_jeniskk");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['id_cardtype']] = $row['cardtype'];
 }
 }
 return $data;
 }

/* Function tampilDatacombo()
	{
		$query=$this->db->get('t_bank');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
} */
Function tambah()
	{
		$noktp = $_POST['noktp'];
			$nokk = $_POST['nokk'];
			$id_bank = $_POST['id_bank'];
			$id_cardtype = $_POST['typekk'];
			$expiredate = $_POST['tglexpire'];
			$limit = $_POST['limit'];
			$data = array(

			'noktp' => $noktp,
			'nokk' => $nokk,
			'id_bank' => $id_bank,
			'id_cardtype' => $id_cardtype,
			'expiredate' => $expiredate,
			'limit' => $limit
			
			
			);
			$res = $this->db->insert('t_kk',$data);
			 
			redirect('../home/daftarccsi');
	}
	
	public function hapus_d($id){ 
            $this->db->where('nokk',$id); 
            $this->db->delete('t_kk'); 
        } 
		
		  public function get_edit($id) { 
                $this->db->where('nokk',$id); 
                $query = $this->db->get('t_kk'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }

		 function edit() { 
		   $noktp = $this->input->post('noktp');
            $id = $this->input->post('nokk'); 
			$id_bank = $this->input->post('bank');
			$id_cardtype = $this->input->post('typekk');
			$expiredate = $this->input->post('tglexpire');
			$limit = $this->input->post('limit');
			
            $data = array ( 
			
			'noktp' => $this->input->post('noktp'),
			'nokk' => $this->input->post('nokk'),
			'id_bank' => $this->input->post('bank'),
			'id_cardtype' => $this->input->post('typekk'),
			'expiredate' => $this->input->post('tglexpire'),
			'limit' => $this->input->post('limit')
		
            ); 
            $this->db->where('nokk',$id); 
            $this->db->update('t_kk',$data); 
        }
}