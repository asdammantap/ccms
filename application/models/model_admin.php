<?php 
	Class Model_admin extends CI_Model {

		Function cek_user($data) {
			$query = $this->db->get_where('t_pengguna', $data);
			return $query;
		}

		/* public function get_all(){
		$data =  $this->db->query('select * from view_kk');
		 return $data;

		} */
		Function tampilData()
	{
		$query=$this->db->get('view_kk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
	Function tampilDatamember()
	{
		$query=$this->db->get('t_anggota');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
Function tampilDatajeniskk()
	{
		$query=$this->db->get('t_jeniskk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
Function tampilDatabank()
	{
		$query=$this->db->get('t_bank');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}

Function tampilDataperid($id)
	{
		$this->db->where('nokk',$id); 
		$query=$this->db->get('view_kk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}

Function tampilDatapribadi($id)
	{
		$this->db->where('noktp',$id); 
		$query=$this->db->get('t_anggota');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}

Function tampilDatawidget()
	{
		$this->db->group_by('cardtype');
		$query=$this->db->get('view_kk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}
Function tampilDatawidgetlimit()
	{
		$this->db->group_by('batas');
		$query=$this->db->get('view_limitkk');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
}

function getbank()
 {
 $query = $this->db->query("SELECT * FROM t_bank");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['id_bank']] = $row['nama_bank'];
 }
 }
 return $data;
 }
 
 function getcardtype()
 {
 $query = $this->db->query("SELECT * FROM t_jeniskk");
 
 if ($query->num_rows()> 0){
 foreach ($query->result_array() as $row)
 {
 $data[$row['id_cardtype']] = $row['cardtype'];
 }
 }
 return $data;
 }

/* Function tampilDatacombo()
	{
		$query=$this->db->get('t_bank');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
} */
Function tambah()
	{
		$noktp = $_POST['noktp'];
			$nokk = $_POST['nokk'];
			$id_bank = $_POST['id_bank'];
			$id_cardtype = $_POST['typekk'];
			$expiredate = $_POST['tglexpire'];
			$limit = $_POST['limit'];
			$data = array(

			'noktp' => $noktp,
			'nokk' => $nokk,
			'id_bank' => $id_bank,
			'id_cardtype' => $id_cardtype,
			'expiredate' => $expiredate,
			'limit' => $limit
			
			
			);
			$res = $this->db->insert('t_kk',$data);
			 
			redirect('../admin/home/daftarccsi');
	}
	public function hapus_bank($id){ 
            $this->db->where('id_bank',$id); 
            $this->db->delete('t_bank'); 
        } 
	public function hapus_kindcc($id){ 
            $this->db->where('id_cardtype',$id); 
            $this->db->delete('t_jeniskk'); 
        } 
	public function hapus_d($id){ 
            $this->db->where('nokk',$id); 
            $this->db->delete('t_kk'); 
        }
public function hapus_ccmember($id){ 
            $this->db->where('noktp',$id); 
            $this->db->delete('t_kk'); 
        }		
		public function hapus_member($id){
            $this->db->where('noktp',$id);
            $this->db->delete('t_anggota'); 
        } 
		
		  public function get_edit($id) { 
                $this->db->where('nokk',$id); 
                $query = $this->db->get('t_kk'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		public function get_edituser($id) { 
                $this->db->where('noktp',$id); 
                $query = $this->db->get('t_anggota'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		 public function get_editbank($id) { 
                $this->db->where('id_bank',$id); 
                $query = $this->db->get('t_bank'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		 public function get_editkindcc($id) { 
                $this->db->where('id_cardtype',$id); 
                $query = $this->db->get('t_jeniskk'); 
                if($query ->num_rows > 0) 
            return $query; 
            else 
            return null; 
        }
		 function edit() { 
		   $noktp = $this->input->post('noktp');
            $id = $this->input->post('nokk'); 
			$id_bank = $this->input->post('id_bank');
			$id_cardtype = $this->input->post('typekk');
			$expiredate = $this->input->post('tglexpire');
			$limit = $this->input->post('limit');
			
            $data = array ( 
			
			'noktp' => $this->input->post('noktp'),
			'nokk' => $this->input->post('nokk'),
			'id_bank' => $this->input->post('id_bank'),
			'id_cardtype' => $this->input->post('typekk'),
			'expiredate' => $this->input->post('tglexpire'),
			'limit' => $this->input->post('limit')
		
            ); 
            $this->db->where('nokk',$id); 
            $this->db->update('t_kk',$data); 
        }
		function editmember() { 
		   $id = $this->input->post('noktp');
            $nama = $this->input->post('nama'); 
			$alamat = $this->input->post('alamat');
			$email = $this->input->post('email');
			$notelp = $this->input->post('notelp');
			$jeniskel = $this->input->post('jeniskel');
			$tmptlahir = $this->input->post('tmptlahir'); 
			$tgllahir = $this->input->post('tgllahir');
			$pekerjaan = $this->input->post('pekerjaan');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
            $data = array ( 
			
			'noktp' => $this->input->post('noktp'),
			'nama' => $this->input->post('nama'),
			'alamat' => $this->input->post('alamat'),
			'email' => $this->input->post('email'),
			'notelp' => $this->input->post('notelp'),
			'jeniskel' => $this->input->post('jeniskel'),
			'tmptlahir' => $this->input->post('tmptlahir'),
			'tgllahir' => $this->input->post('tgllahir'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'username' => $this->input->post('username'),
			'password' => md5($password)
		
            ); 
            $this->db->where('noktp',$id); 
            $this->db->update('t_anggota',$data); 
        }
		 function edit_bank() { 
		   $id = $this->input->post('id_bank'); 
			$nama_bank = $this->input->post('nama_bank');
			$cabang = $this->input->post('cabang');
			
            $data = array ( 
			
			'id_bank' => $this->input->post('id_bank'),
			'nama_bank' => $this->input->post('nama_bank'),
			'cabang' => $this->input->post('cabang')
            ); 
            $this->db->where('id_bank',$id); 
            $this->db->update('t_bank',$data); 
        }
		 function editkindcc() { 
		   $id = $this->input->post('id_cardtype'); 
			$cardtype = $this->input->post('cardtype');
			$data = array ( 
			
			'id_cardtype' => $this->input->post('id_cardtype'),
			'cardtype' => $this->input->post('cardtype')); 
            $this->db->where('id_cardtype',$id); 
            $this->db->update('t_jeniskk',$data); 
        }
}