<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Wong Mantap">
    <title>Credit Card Management System</title>
    <!-- Bootstrap core CSS -->
	<link rel="icon" type="image/x-icon" href="assets/images/logoccmstrans.png">
    <link href="assets/css/skeleton.css" rel="stylesheet">
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<link href="assets/js/bootstrap.js" rel="stylesheet">
	<!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
  </head>

  <body>
   <div class="container">
<EMBED SRC="assets/music/welcomesounddipake.wav" AUTOSTART="TRUE" LOOP="false" WIDTH="1" HEIGHT="1" ALIGN="CENTER"></EMBED>
      <!-- Main component for a primary marketing message or call to action -->
      <div class="welcomebox">
        <center><img src="assets/images/welcome.jpg" height="250px" width="100%"></center>
	  </div>
        <p>
          <a class="button button-primary" href="home/loginform" role="button" style="margin-top:20px;float:right;">Please Login &raquo;</a>
        </p>
    </div> <!-- /container -->