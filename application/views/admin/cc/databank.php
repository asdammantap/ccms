<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator | View Data Bank</title>
  <link rel="icon" type="image/x-icon" href="../../assets/images/logoccmstrans.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="../../assets/admins/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../assets/admins/font-awesome-4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../assets/admins/bootstrap/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/admins/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../assets/admins/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../assets/admins/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../../assets/admins/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../../assets/admins/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../assets/admins/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../assets/admins/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/admins/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="../../assets/css/style.css">
  <link href="../../assets/css/bootstrap.css" rel="stylesheet">
	<link href="../../assets/js/bootstrap.js" rel="stylesheet">
	  <link rel="stylesheet" href="../../assets/csspart2/media-queries.css">		
		<link rel="stylesheet" href="../../assets/assetpart2/css/bootstrap-responsive.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
		table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

@media (max-width: 30em){

    table.responsive-table{
      box-shadow: none;  
	  width:100%;
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2),
  table.responsive-table td:nth-child(3),
  table.responsive-table td:nth-child(4),
  table.responsive-table td:nth-child(5){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before{
    content: 'No. KK = ';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Bank = ';
  }
  table.responsive-table td:nth-child(3):before{
    content: 'Type = ';
  }
  table.responsive-table td:nth-child(4):before{
    content: 'Expire = ';
  }
  table.responsive-table td:nth-child(5):before{
    content: 'Limit = ';
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before,
  table.responsive-table td:nth-child(3):before,
  table.responsive-table td:nth-child(4):before,
  table.responsive-table td:nth-child(5):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CC</b>MS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>&nbsp;CCMS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../assets/admins/dist/img/user.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">ADMIN</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../assets/admins/dist/img/user.jpg" class="img-circle" alt="User Image">

                <p>
                  ADMIN OF CCMS
                  <small>CREDIT CARD MANAGEMENT SYSTEM</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="profil" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="./logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
         </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../assets/admins/dist/img/user.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Administrator</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
       <li class="active">
          <a href="./adminpage">
            <i class="fa fa-home"></i> <span>Dashboard</span> </a>
          </li>
       <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Member</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="./listmember"><i class="fa fa-eye"></i>View Data</a></li>
            <li><a href="./member"><i class="fa fa-plus"></i>Input Data</a></li>
           </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-credit-card"></i>
            <span>Credit Card</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="./kindofcc"><i class="fa fa-eject"></i>Kind Of CC</a></li>
            <li><a href="./daftarcc"><i class="fa fa-credit-card"></i>Data CC</a></li>
            <li><a href="./daftarbank"><i class="fa fa-building"></i>Data Bank</a></li>
           </ul>
        </li>
        <li>
          <a href="">
            <i class="fa fa-calculator"></i> <span>Transaction</span>
           </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color:white;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="./adminpage"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Data Bank</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content" style="background-color:white;">
      <!-- Small boxes (Stat box) -->
      <div class="row" style="background-color:white;">
       <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <div class="tab-content no-padding">
              <div class="span10" style="height:auto;display:inline-block;margin-top:10px;">
			   <div style="float:left;margin-bottom:10px;"><a class="btn btn-primary" href="./bank" role="button">Tambah Data&nbsp;&nbsp;<span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></div>
   <table class="layout display responsive-table">
    <thead>
        <tr>
			<th style="width:20%;">ID Bank</th>   
            <th>Bank</th>                        
            <th style="width:15%;">Cabang</th>
			<th style="width:20%;" colspan="3"><center>Action</th>						                         
        </tr>
    </thead>
    <tbody>
	<?php 
foreach ($data as $row) {	 
     echo '<tr>';		
      echo '<td>'.
	  $row->id_bank.
	  '</td>';
									echo '<td>'.$row->nama_bank.'</td>';
                                    echo '<td>'.$row->cabang.'</td>';
								   echo '<td><a class="btn btn-primary btn-xs" href=bankupdate/'.$row->id_bank.'>Edit</a></td>';
									echo '<td><a class="btn btn-danger btn-xs" href=deletebank/'.$row->id_bank.'>Delete</a></td>';
									echo'</tr>'; }?>
	 
    </tbody>
</table>
</div>
              </div>
          </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
      
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b>1.0.0
    </div>
    <strong>Copyright &copy; 2015&nbsp;<a href="">CCMS Studio</a>.</strong> All rights
    reserved.
  </footer>
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="../../assets/admins/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="../../assets/admins/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../../assets/admins/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="../../assets/admins/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../../assets/admins/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../../assets/admins/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../../assets/admins/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="../../assets/admins/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../../assets/admins/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../../assets/admins/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../../assets/admins/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../assets/admins/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../assets/admins/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../../assets/admins/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../assets/admins/dist/js/demo.js"></script>
</body>
</html>