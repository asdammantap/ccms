<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Wong Mantap">
    <title>CCMS - Login</title>
    <!-- Bootstrap core CSS -->
	<link rel="icon" type="image/x-icon" href="../../assets/images/logoccmstrans.png">
    <link rel="stylesheet" href="../../assets/css/csslogin/style.css">
	<link href="../assets/css/bootstrap.css" rel="stylesheet">
	<link href="../assets/js/bootstrap.js" rel="stylesheet">
	
   </head>

  <body>
 
    <div class="alert alert-error alert-dismissible" role="alert">
	<button class="close" data-dismiss="alert" style="float:right;"><span aria-hidden="true">&times;</span></button>
				  <strong>GAGAL MELAKUKAN LOGIN !!</br>
				  SILAHKAN COBA KEMBALI !!
	</div>
   <section>
<div class="span"></div>
  <h1>Credit Card Management System</h1>
  <div class="textatas"><h5>Sign in To Your CCMS Account</h5></div>
  
<?php echo form_open("./auth/cek_login"); ?>
    <input placeholder='Username' type='text' name='username'>
    <input placeholder='Password' type='password' name='password'>
	<input type="submit" value="LOGIN">
  <?php echo form_close(); ?>
  <?php echo $this->session->flashdata('gagal') ?>
 <div class="textlupa"><a href="lupapassword.php">Forgot Password</a></div>
<center>
</br><font style="color:blue;">NEW MEMBER OF CCMS ?</font>
<h2>
<a href="../../home/daftarmember">SIGN UP FOR A NEW ACCOUNT</a>
</h2>
 </section>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../../assets/jquery.min.js"></script>    
<!-- Latest compiled and minified JavaScript -->
<script src="../../assets/js/bootstrap.min.js"></script>