<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#" style="margin-left:10px;margin-bottom:10px;"><span><img src="../assets/images/logoccmstrans.png" style="height:30px;"></span>
	  <font style="font-family:arial;font-weight:bold;">CCMS</font></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    
      
      <ul class="nav navbar-nav navbar-right" style="margin-top:6px;">
        <li style="margin-left:15px;"><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;Home</a></li>
		<li style="margin-left:15px;"><a href="../home/daftarcc"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span>&nbsp;Credit Card</a></li>
        <li class="dropdown" style="margin-left:15px;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
		  <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo $username; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-top:4px;">
            <li><a href="" target="_blank">Profil <span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="memberpage/logout">Logout <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

