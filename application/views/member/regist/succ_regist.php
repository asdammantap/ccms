<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Wong Mantap">
    <title>Buat Akun CCMS</title>
    <!-- Bootstrap core CSS -->
	<link rel="icon" type="image/x-icon" href="../assets/images/logoccmstrans.png">
	
	 <link rel="stylesheet" href="../../assets/csspart2/normalize.min.css">
      <link rel="stylesheet" href="../../assets/csspart2/main.css">		
     
	  <link rel="stylesheet" href="../../assets/csspart2/media-queries.css">		
		<link rel="stylesheet" href="../../assets/csspart2/bootstrap.css">
		
	  
		<link rel="stylesheet" href="../../assets/assetpart2/css/bootstrap.css">	
		<link rel="stylesheet" href="../../assets/assetpart2/css/bootstrap-responsive.css">
	</head>

  <body>
  
  <div class="span12" style="width:95%;margin-top:10px;float:left;margin-bottom:30px;"><img src="../../assets/images/logoccmstrans.png" style="width:50px;height:50px;"><font style="font-size:12pt;font-weight:bold;text-transform:uppercase;margin-left:20px;">Credit Card Management System</font><a class="btn btn-primary" href="../../home/loginform" role="button" style="margin-top:20px;float:right;">Sign in &raquo;</a>
</div>
<div class="span12" style="width:95%;margin-top:10px;float:left;margin-bottom:30px;">
<div class="alert alert-success alert-dismissible" role="alert">
				  <strong>SELAMAT ANDA BERHASIL MELAKUKAN REGISTRASI !</br>
				  SILAHKAN LOGIN !!
				</div>
				</div>

<center>
	
   <div class="span3" style="height:auto;display:inline-block;margin-top:10px;border:1px solid #abb4c2;
  box-shadow: 1px 1px 2px rgba(0,0,0,.3);">
  
   <?php echo form_open("regist/regist_member"); ?>
    <table border="0" style="float:left;margin-left:30px;">
	<tr><td>&nbsp;</td></tr>
	<tr><td>No. KTP:</td></tr>
    <tr><td><input placeholder='Masukkan No. KTP' type='text' name='noktp'></td></tr>
	<tr><td>Nama Lengkap:</td></tr>
    <tr><td><input placeholder='Masukkan Nama Lengkap' type='text' name='nama'></td></tr>
	<tr><td>Alamat:</td></tr>
    <tr><td><textarea placeholder='Masukkan Alamat' name='alamat'></textarea></td></tr>
	<tr><td>Email:</td></tr>
    <tr><td><input placeholder='Masukkan Email' type='text' name='email'></td></tr>
	<tr><td>No Telepon:</td></tr>
    <tr><td><input placeholder='Masukkan No. Telp' type='text' name='notelp'></td></tr>
	<tr><td>Jenis Kelamin:</td></tr>
    <tr><td>
	<select name='jeniskel'>
	<option value='laki-laki'>Laki-Laki</option>
	<option value='perempuan'>Perempuan</option>
	</select>
	</td></tr>
	<tr><td>Tempat Lahir:</td></tr>
    <tr><td><input placeholder='Masukkan Tempat Lahir' type='text' name='tmptlahir'></td></tr>
	<tr><td>Tanggal Lahir:</td></tr>
    <tr><td><input placeholder='Tahun-Bulan-Tanggal' type='text' name='tgllahir'></td></tr>
	<tr><td>Pekerjaan:</td></tr>
    <tr><td><input placeholder='Masukkan Pekerjaan' type='text' name='pekerjaan'></td></tr>
	<tr><td>Username:</td></tr>
    <tr><td><input placeholder='Masukkan Username' type='text' name='username'></td></tr>
	<tr><td>Password:</td></tr>
    <tr><td><input placeholder='Masukkan Password' type='password' name='password'></td></tr>
	<tr><td><input type="submit" class="btn btn-primary" value="Sign Up >>"></td></tr>
	<tr><td>&nbsp;</td></tr>
	</table>
  <?php echo form_close(); ?>
  <?php echo $this->session->flashdata('gagal') ?>
 </div>
 <div class="span10" style="height:auto;display:inline-block;margin-top:10px;">
  <font style="font-family:comic-sans;"><h3>Make Your Credit Card Activity Easiest Than Before</h3></font></br>
  <img src="../../assets/images/creditcard.png" style="width:200px;height:200px;"></br>
  <font style="font-family:comic-sans;"><h3>One Account For Every Credit Card</h3>
  <span style="height:auto;display:inline-block;margin-bottom:20px;width:100%;">
  <img src="../../assets/images/cc/visa.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/mastercard.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/maestro.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/paypa.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/amex.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/dankort.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/diners.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/forbru.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/google.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/jcb.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/laser.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/shopify.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/solo.png" style="width:30px;height:30px;">
  <img src="../../assets/images/cc/discover.png" style="width:30px;height:30px;"></font></br>
  </div>
  </div>