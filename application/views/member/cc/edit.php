<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Wong Mantap">
    <title>Credit Card Management System</title>
    <!-- Bootstrap core CSS -->
	<link rel="icon" type="image/x-icon" href="../../assets/images/logoccmstrans.png">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
	<link href="../../assets/js/bootstrap.js" rel="stylesheet">
	  <link rel="stylesheet" href="../../assets/csspart2/media-queries.css">		
		<link rel="stylesheet" href="../../assets/assetpart2/css/bootstrap-responsive.css">
		<style>
		table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

@media (max-width: 30em){

    table.responsive-table{
      box-shadow: none;  
	  width:100%;
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2),
  table.responsive-table td:nth-child(3),
  table.responsive-table td:nth-child(4),
  table.responsive-table td:nth-child(5){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before{
    content: 'No. KK =';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Bank =';
  }
  table.responsive-table td:nth-child(3):before{
    content: 'Type =';
  }
  table.responsive-table td:nth-child(4):before{
    content: 'Expire =';
  }
  table.responsive-table td:nth-child(5):before{
    content: 'Limit =';
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before,
  table.responsive-table td:nth-child(3):before,
  table.responsive-table td:nth-child(4):before,
  table.responsive-table td:nth-child(5):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
}
</style>
	</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#" style="margin-left:10px;margin-bottom:10px;"><span><img src="../../assets/images/logoccmstrans.png" style="height:30px;"></span>
	  <font style="font-family:arial;font-weight:bold;">CCMS</font></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    
      
      <ul class="nav navbar-nav navbar-right" style="margin-top:6px;">
        <li style="margin-left:15px;"><a href="../memberpage"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;Home</a></li>
		<li style="margin-left:15px;"><a href="../daftarcc"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span>&nbsp;Credit Card</a></li>
        <li class="dropdown" style="margin-left:15px;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
		  <span class="glyphicon glyphicon-user" aria-hidden="true"></span><?php echo $username; ?><span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-top:4px;">
            <li><a href="" target="_blank">Profil <span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="../member/memberpage/logout">Logout <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<button type="button" class="btn btn-success tambah pull-left" data-toggle="modal" data-target="#myModaledit" style="margin-left:40px;">
  Edit Data &nbsp;<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
</button>
</br>

<!-- Modal Edit -->
<div class="modal fade" id="myModaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
		
		<?php echo form_open_multipart('../home/proses_edit');?>
		<?php foreach($data->result() as $edit):?>
		    <input type="text" required  value="<?php echo($edit->noktp);?>" name="noktp" class="form-control" placeholder="No. KTP">
			<br>
			<input type="text" required  value="<?php echo($edit->nokk);?>" name="nokk" class="form-control" placeholder="No. KK">
			<br>
			<input type="text" required  value="<?php echo($edit->id_bank);?>" name="bank" class="form-control" placeholder="Bank">
			<br>
			<input type="text" required  value="<?php echo($edit->id_cardtype);?>" name="typekk" class="form-control" placeholder="Type KK">
			<br>
			<input type="text" required  value="<?php echo($edit->expiredate);?>" name="tglexpire" class="form-control" placeholder="Tanggal Expire">
			<br>
			<input type="text" required  value="<?php echo($edit->limit);?>" name="limit" class="form-control" placeholder="Limit">
			<br>
			<input type="submit" name="btnsimpan" class="btn btn-primary btn-sm" value="Simpan Data">
			<?php endforeach; ?> 
		</form>
			
		</div>
      </div>
      <div class="modal-footer">
      
      </div>
    </div>
  </div>
</div>




	
	<br><center>
   <div class="span10" style="height:150px;display:inline-block;margin-top:10px;border:1px solid #abb4c2;
  box-shadow: 1px 1px 2px rgba(0,0,0,.3);">
   <table class="layout display responsive-table">
    <thead>
        <tr>
			<th style="width:20%;">Credit Card Number</th>   
            <th>Bank</th>                        
            <th style="width:15%;">Card Type</th>  
			<th>Limit</th>
			<th>Expire Date</th>															
			</tr>
    </thead>
    <tbody>
	<?php 
foreach ($datatampil as $row) {	 
     echo '<tr>';		
      echo '<td>'.
	  $row->nokk.
	  '</td>';
									echo '<td>'.$row->nama_bank.'</td>';
                                    echo '<td>'.$row->cardtype.'</td>';
									
                                    echo '<td>'.$row->batas.'</td>';
									echo '<td>'.$row->expiredate.'</td>';
									 echo'</tr>';
	 }
	 ?>
    </tbody>
</table>
</div>
  <div class="span3" style="margin-top:10px;">
  <div style="position:fiexed" class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Credit Card Are Often Used</h3>
                  </div>
                  
                  <div class="panel-body">
                    <?php 
foreach ($datatampil as $row) {	 echo $row->cardtype;
echo '</p>'; } ?>
                  </div>
                </div>
 </div>
 <div class="span3" style="margin-top:10px;">
  <div style="position:fiexed" class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Credit Card Minimum Limit</h3>
                  </div>
                  
                  <div class="panel-body">
				  <table class="layout display responsive-table">
    <thead>
        <tr>
			<th style="width:20%;">Credit Card Number</th>   
            <th>Limit</th>
			</tr>
    </thead>
    <tbody>
                    <?php 
foreach ($datatampilwidgetlimit as $row) {	 echo '<tr><td>'.$row->nokk;
echo '</td>';
echo '<td>'.$row->batas.'</td>';
echo '</tr>'; } ?>
 </tbody>
</table>
</br>
                  </div>
                </div>
 </div>
     <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../../assets/js/bootstrap.min.js"></script>

</body>
</html>