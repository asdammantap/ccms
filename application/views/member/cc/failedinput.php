<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Wong Mantap">
    <title>Credit Card Management System</title>
    <!-- Bootstrap core CSS -->
	<link rel="icon" type="image/x-icon" href="../assets/images/logoccmstrans.png">
	<link href="../assets/css/bootstrap.css" rel="stylesheet">
	<link href="../assets/js/bootstrap.js" rel="stylesheet">
	  <link rel="stylesheet" href="../assets/csspart2/media-queries.css">		
		<link rel="stylesheet" href="../assets/assetpart2/css/bootstrap-responsive.css">
		<style>
		table.layout{
  width: 100%;
  border-collapse: collapse;
}
table.display{
  margin: 1em 0;
}
table.display th,
table.display td{
  border: 1px solid #B3BFAA;
  padding: .5em 1em;
}

table.display th{ background: #D5E0CC; }
table.display td{ background: #fff; }

table.responsive-table{
  box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
}

@media (max-width: 30em){

    table.responsive-table{
      box-shadow: none;  
	  width:100%;
    }
    table.responsive-table thead{
      display: none; 
    }
  table.display th,
  table.display td{
    padding: .5em;
  }
  table.responsive-table td:nth-child(1),
  table.responsive-table td:nth-child(2),
  table.responsive-table td:nth-child(3),
  table.responsive-table td:nth-child(4),
  table.responsive-table td:nth-child(5){
    padding-left: 25%;
  }
  table.responsive-table td:nth-child(1):before{
    content: 'No. KK = ';
  }
  table.responsive-table td:nth-child(2):before{
    content: 'Bank = ';
  }
  table.responsive-table td:nth-child(3):before{
    content: 'Type = ';
  }
  table.responsive-table td:nth-child(4):before{
    content: 'Expire = ';
  }
  table.responsive-table td:nth-child(5):before{
    content: 'Limit = ';
  }
  table.responsive-table td:nth-child(1):before,
  table.responsive-table td:nth-child(2):before,
  table.responsive-table td:nth-child(3):before,
  table.responsive-table td:nth-child(4):before,
  table.responsive-table td:nth-child(5):before{
    position: absolute;
    left: .5em;
    font-weight: bold;
  }
  
    table.responsive-table tr,
    table.responsive-table td{
        display: block;
    }
    table.responsive-table tr{
        position: relative;
        margin-bottom: 1em;
    box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
    }
    table.responsive-table td{
        border-top: none;
    }
    table.responsive-table td.organisationnumber{
        background: #D5E0CC;
        border-top: 1px solid #B3BFAA;
    }
    table.responsive-table td.actions{
        position: absolute;
        top: 0;
        right: 0;
        border: none;
        background: none;
    }
}
</style>
	</head>
<body>
<?php echo $this->load->view('member/cc/menu/nav'); ?>
<div class="alert alert-error alert-dismissible" role="alert">
	<button class="close" data-dismiss="alert" style="float:right;"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Gagal Ditambahkan</strong>
	</div>
<button type="button" class="btn btn-success tambah pull-left" data-toggle="modal" data-target="#myModal" style="margin-left:40px;">
  Tambah Data&nbsp;&nbsp;<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
</button>
	
<br>
<br>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Masukan Data</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
		
		<?php echo form_open_multipart('../home/insertcc');?>
		    <input type="text" required  name="noktp" class="form-control" placeholder="No. KTP">
			<br>
			<input type="text" required  name="nokk" class="form-control" placeholder="No. KK">
			<br>
			<select  name="id_bank" class="form-control" placeholder="Bank">
			<option selected>--Pilih Bank--</option>
			<option value=<?=form_dropdown('id_bank',$datatampilcombo);?></option>
			</select>
			<br>
			<select  name="typekk" class="form-control" placeholder="Type KK">
			<option selected>--Pilih Jenis Kartu Kredit--</option>
			<option value=<?=form_dropdown('id_cardtype',$datatampilcombojeniskk);?></option>
			</select>
			<br>
			<input type="text" required  name="tglexpire" class="form-control" placeholder="Tanggal Expire">
			<br>
			<input type="text" required  name="limit" class="form-control" placeholder="Limit">
			<br>
			<input type="submit" name="btnsimpan" class="btn btn-primary btn-sm" value="Simpan Data">
		</form>
			
		</div>
      </div>
      <div class="modal-footer">
      
      </div>
    </div>
  </div>
</div>



	
	<br><center>
   <div class="span10" style="height:auto;display:inline-block;margin-top:10px;border:1px solid #abb4c2;
  box-shadow: 1px 1px 2px rgba(0,0,0,.3);">
   <table class="layout display responsive-table">
    <thead>
        <tr>
			<th style="width:20%;">Credit Card Number</th>   
            <th>Bank</th>                        
            <th style="width:15%;">Card Type</th>
			<th>Limit</th>
			<th>Expire Date</th>															
			<th style="width:20%;">Action</th>						                         
        </tr>
    </thead>
    <tbody>
	<?php 
foreach ($data as $row) {	 
     echo '<tr>';		
      echo '<td>'.
	  $row->nokk.
	  '</td>';
									echo '<td>'.$row->nama_bank.'</td>';
                                    echo '<td>'.$row->cardtype.'</td>';
									
                                    echo '<td>'.$row->limit.'</td>';
									echo '<td>'.$row->expiredate.'</td>';
									echo '<td><a class="btn btn-primary btn-xs" href=editcc/'.$row->nokk.'>EDIT</a>&nbsp;
									<a class="btn btn-danger btn-xs" href=deletecc/'.$row->nokk.'>DELETE</a></td>';
									 echo'</tr>';
	 }
	 ?>
    </tbody>
</table>
</div>
  <div class="span3" style="margin-top:10px;">
  <div style="position:fiexed" class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Credit Card Are Often Used</h3>
                  </div>
                  
                  <div class="panel-body">
                    <?php 
foreach ($datatampilwidget as $row) {	 echo $row->cardtype; } ?></br>
                  </div>
                </div>
 </div>
 
      <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>

</body>
</html>