<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Wong Mantap">
    <title>Credit Card Management System</title>
    <!-- Bootstrap core CSS -->
	<link rel="icon" type="image/x-icon" href="../../assets/images/logoccmstrans.png">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
	<link href="../../assets/js/bootstrap.js" rel="stylesheet">
	  <link rel="stylesheet" href="../../assets/csspart2/media-queries.css">		
		<link rel="stylesheet" href="../../assets/assetpart2/css/bootstrap-responsive.css">
	</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#" style="margin-left:10px;margin-bottom:10px;"><span><img src="../../assets/images/logoccmstrans.png" style="height:30px;"></span>
	  <font style="font-family:arial;font-weight:bold;">CCMS</font></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    
      
      <ul class="nav navbar-nav navbar-right" style="margin-top:6px;">
        <li style="margin-left:15px;"><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;Home</a></li>
		<li style="margin-left:15px;"><a href="../daftarcc"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span>&nbsp;Credit Card</a></li>
        <li class="dropdown" style="margin-left:15px;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
		  <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php foreach ($datatampilprofil as $row) { echo $row->username; }?> <span class="caret"></span></a>
          <ul class="dropdown-menu" style="margin-top:4px;">
            <li><a href="" target="_blank">Profil <span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></li>
            <li role="separator" class="divider"></li>
            <li><a href="../logout">Logout <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



<center>
   <div class="span10" style="height:auto;display:inline-block;margin-top:10px;border:1px solid #abb4c2;
  box-shadow: 1px 1px 2px rgba(0,0,0,.3);">
   <div style="margin-bottom:5px;margin-left:20px;margin-right:20px;"><font style="font-family:comic-sans;"><h3 style="font-weight:bold;">PROFIL</h3></font></div>
   <div style="margin-left:20px;margin-right:20px;"><font style="font-family:comic-sans;text-align:justify;"><h4><?php 
foreach ($datatampilprofil as $row) {	 
echo 'No. KTP = '.$row->noktp;
echo '</p>';
echo 'Nama = '.$row->nama;
echo '</p>';
echo 'Alamat = '.$row->alamat;
echo '</p>';
echo 'No. Telephone = '.$row->notelp;
echo '</p>';
echo 'Email = '.$row->email;
echo '</p>';
echo 'Jenis Kelamin = '.$row->jeniskel;
echo '</p>';
echo 'Tempat Tanggal Lahir = '.$row->tmptlahir.'&nbsp;,&nbsp;'.$row->tgllahir;
echo '</p>';
echo 'Pekerjaan = '.$row->pekerjaan;
} ?></h4></font></div>
  </br>
 </div>
  <div class="span3" style="margin-top:10px;">
  <div style="position:fiexed" class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Data Kartu Kredit</h3>
                  </div>
                  
                  <div class="panel-body">
                    Visa
                  </div>
                </div>
 </div>
 
      <script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../../assets/js/bootstrap.min.js"></script>

</body>
</html>
