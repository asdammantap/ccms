<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Wong Mantap">
    <title>Credit Card Management System</title>
    <!-- Bootstrap core CSS -->
	<link rel="icon" type="image/x-icon" href="../assets/images/logoccmstrans.png">
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
	<link href="../assets/js/bootstrap.js" rel="stylesheet">
	  <link rel="stylesheet" href="../assets/csspart2/media-queries.css">		
		<link rel="stylesheet" href="../assets/assetpart2/css/bootstrap-responsive.css">
	</head>
<body>
<?php echo $this->load->view('template/nav'); ?>
<center>
   <div class="span10" style="height:auto;display:inline-block;margin-top:10px;border:1px solid #abb4c2;
  box-shadow: 1px 1px 2px rgba(0,0,0,.3);">
   <div style="margin-bottom:5px;margin-left:20px;margin-right:20px;"><font style="font-family:comic-sans;"><h3 style="font-weight:bold;">CREDIT CARD MANAGEMENT SYSTEM</h3></font></div>
   <div style="margin-left:20px;margin-right:20px;"><font style="font-family:comic-sans;text-align:justify;"><h4>Sistem Manajemen Kartu Kredit ini dapat memudahkan pengguna kartu kredit untuk mengelola data kartu kredit yang dimiliki mulai dari limit kartu, expiry date, sampai dengan jumlah pemakaian perbulan</h4></font></div>
  <font style="font-family:comic-sans;"><h3>One Account For Every Credit Card</h3>
  <span style="height:auto;display:inline-block;margin-bottom:20px;width:100%;">
  <img src="../assets/images/cc/visa.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/mastercard.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/maestro.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/paypa.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/amex.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/dankort.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/diners.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/forbru.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/google.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/jcb.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/laser.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/shopify.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/solo.png" style="width:30px;height:30px;">
  <img src="../assets/images/cc/discover.png" style="width:30px;height:30px;"></font></br>
 </div>
  <div class="span3" style="margin-top:10px;">
  <div style="position:fiexed" class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">Data Kartu Kredit</h3>
                  </div>
                  
                  <div class="panel-body">
                    Visa
                  </div>
                </div>
 </div>
 
      <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>

</body>
</html>
