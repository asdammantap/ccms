<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function index()
	{
		$this->load->view('home');
	}
	public function admccms()
	{
		$this->load->view('admin/login.php');
	}
	public function memberpage()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('member/index', $data);
	}
	public function loginform()
	{
		$this->load->view('login');
	}
	public function daftarmember()
	{
		$this->load->view('daftar');
	}
	public function regist_member()
	{
		$this->load->view('regist');
	}
	public function profil($id)
	{
	    $data['noktp'] = $this->session->userdata('noktp');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_user');
		$data['datatampilprofil']=$this->model_user->tampilDatapribadi($id);
		$this->load->view('member/profil', $data);
	}
	public function daftarcc()
	{
	    $data['noktp'] = $this->session->userdata('noktp');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_user');
		$data['data']=$this->model_user->tampilData();
		$data['datatampilwidget']=$this->model_user->tampilDatawidget();
		$data['datatampilwidgetlimit']=$this->model_user->tampilDatawidgetlimit();
		$data['datatampilcombo']=$this->model_user->getbank();
		$data['datatampilcombojeniskk']=$this->model_user->getcardtype();
		$this->load->view('member/cc/datacc', $data);
	}
	public function daftarccsi()
	{
	    $data['noktp'] = $this->session->userdata('noktp');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_user');
		$data['data']=$this->model_user->tampilData();
		$data['datatampilwidget']=$this->model_user->tampilDatawidget();
		$data['datatampilwidgetlimit']=$this->model_user->tampilDatawidgetlimit();
		$data['datatampilcombo']=$this->model_user->getbank();
		$data['datatampilcombojeniskk']=$this->model_user->getcardtype();
		$this->load->view('member/cc/successinput', $data);
	}
	public function daftarccfi()
	{
	    $data['noktp'] = $this->session->userdata('noktp');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_user');
		$data['data']=$this->model_user->tampilData();
		$data['datatampilwidget']=$this->model_user->tampilDatawidget();
		$data['datatampilcombo']=$this->model_user->getbank();
		$data['datatampilcombojeniskk']=$this->model_user->getcardtype();
		$this->load->view('member/cc/successinput', $data);
	}
	public function insertcc()
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_user');
		$data['data']=$this->model_user->tambah();
		$this->load->view('member/cc/datacc', $data);
	}
	public function deletecc($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('model_user','',TRUE); 
		$data['data']=$this->model_user->hapus_d($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../home/daftarcc');
            }
		$this->load->view('member/cc/datacc', $data);
	}
	
	public function editcc($id) { 
		
           $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_user');
		$data['datatampil']=$this->model_user->tampilDataperid($id);
		$data['datatampilwidgetlimit']=$this->model_user->tampilDatawidgetlimit();
		$data['data']=$this->model_user->get_edit($id);
		$this->load->view('member/cc/edit', $data);

        } 
		
	public function transcc($id) { 
		
           $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_user');
		$data['datatampil']=$this->model_user->tampilDataperidtrans($id);
		$data['datatampilwidgetlimit']=$this->model_user->tampilDatawidgetlimit();
		$data['data']=$this->model_user->get_edit($id);
	    $this->load->view('member/cc/transaction', $data);
		$data['data']=$this->model_user->getkodeunik();
       } 
        public function proses_edit() { 
            $this->load->model('model_user','',TRUE); 
            $this->model_user->edit(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../home/daftarcc');
        }
		
		public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('../home/loginform');
	}

	
	
}
