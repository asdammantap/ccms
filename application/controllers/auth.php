<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	// public function index() {
		// $this->load->view('index');
	// }

	public function index($error = NULL) {
        $data = array(
            'title' => 'Login Page',
            'action' => site_url('auth/login'),
            'error' => $error
        );
        $this->load->view('login', $data);
    }
	
	public function cek_login() {
		$data = array('username' => $this->input->post('username', TRUE),
						'pass' => md5($this->input->post('password', TRUE))
			);
		$this->load->model('model_user'); // load model_user
		$hasil = $this->model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['noktp'] = $sess->noktp;
				$sess_data['username'] = $sess->username;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('level')=='member') {
				redirect('../home/memberpage');
			}		
		}
		else {
			$this->load->view('login/failed_login');
		}
	}
	
	public function cek_loginadmin() {
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => md5($this->input->post('password', TRUE))
			);
		$this->load->model('model_admin'); // load model_user
		$hasil = $this->model_admin->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Login';
				$sess_data['uid'] = $sess->uid;
				$sess_data['noktp'] = $sess->noktp;
				$sess_data['username'] = $sess->username;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('level')=='admin') {
				redirect('../admin/home/adminpage');
			}
			elseif ($this->session->userdata('level')=='member') {
				redirect('../home/memberpage');
			}		
		}
		else {
			$this->load->view('login/failed_login');
		}
	}

}

?>
