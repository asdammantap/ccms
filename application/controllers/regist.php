<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class regist extends CI_Controller {

	public function index() {
		$this->load->view('daftar');
	}
 
           function regist_member()
		{
		
			$noktp = $_POST['noktp'];
			$nama = $_POST['nama'];
			$alamat = $_POST['alamat'];
			$notelp = $_POST['notelp'];
			$email = $_POST['email'];
			$jeniskel = $_POST['jeniskel'];
			$tmptlahir = $_POST['tmptlahir'];
			$tgllahir = $_POST['tgllahir'];
			$pekerjaan = $_POST['pekerjaan'];
			$username = $_POST['username'];
			$password = $_POST['password'];
			$level='member';
			
			$data = array(
			'noktp' => $noktp,
			'nama' => $nama,
			'alamat' => $alamat,
			'notelp' => $notelp,
			'email' => $email,
			'jeniskel' => $jeniskel,
			'tmptlahir' => $tmptlahir,
			'tgllahir' => $tgllahir,
			'pekerjaan' => $pekerjaan,
			'username' => $username,
			'password' => md5($password)
			);
			
			$datapengguna = array(
			'username' => $username,
			'password' => md5($password),
			'level'=> $level
			);
			
			$res = $this->db->insert('t_anggota',$data);
			$resuser = $this->db->insert('t_pengguna',$datapengguna);
			 $this->load->library('form_validation');
		$this->form_validation->set_rules('noktp','noktp','required');
		$this->form_validation->set_rules('nama','nama','required');
		$this->form_validation->set_rules('alamat','alamat','required');
		$this->form_validation->set_rules('notelp','notelp','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('tmptlahir','tmptlahir','required');
		$this->form_validation->set_rules('tgllahir','tgllahir','required');
		$this->form_validation->set_rules('pekerjaan','pekerjaan','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		if($this->form_validation->run()==FALSE){
			$this->load->view('member/regist/failed_regist');
		}else{
			$this->load->view('member/regist/succ_regist');
		}
			
		}
		
		 function add_member()
		{
		
			$noktp = $_POST['noktp'];
			$nama = $_POST['nama'];
			$alamat = $_POST['alamat'];
			$notelp = $_POST['notelp'];
			$email = $_POST['email'];
			$jeniskel = $_POST['jeniskel'];
			$tmptlahir = $_POST['tmptlahir'];
			$tgllahir = $_POST['tgllahir'];
			$pekerjaan = $_POST['pekerjaan'];
			$username = $_POST['username'];
			$password = $_POST['password'];
			$level='member';
			
			$data = array(
			'noktp' => $noktp,
			'nama' => $nama,
			'alamat' => $alamat,
			'notelp' => $notelp,
			'email' => $email,
			'jeniskel' => $jeniskel,
			'tmptlahir' => $tmptlahir,
			'tgllahir' => $tgllahir,
			'pekerjaan' => $pekerjaan,
			'username' => $username,
			'password' => md5($password)
			);
			
			$datapengguna = array(
			'username' => $username,
			'password' => md5($password),
			'level'=> $level
			);
			
			$res = $this->db->insert('t_anggota',$data);
			$resuser = $this->db->insert('t_pengguna',$datapengguna);
			 $this->load->library('form_validation');
		$this->form_validation->set_rules('noktp','noktp','required');
		$this->form_validation->set_rules('nama','nama','required');
		$this->form_validation->set_rules('alamat','alamat','required');
		$this->form_validation->set_rules('notelp','notelp','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('tmptlahir','tmptlahir','required');
		$this->form_validation->set_rules('tgllahir','tgllahir','required');
		$this->form_validation->set_rules('pekerjaan','pekerjaan','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		if($this->form_validation->run()==FALSE){
			$this->load->view('admin/user/failed_regist');
		}else{
			$this->load->view('admin/user/succ_regist');
		}
		}
}