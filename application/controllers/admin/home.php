<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function index()
	{
		$this->load->view('admin/home');
	}
	public function admccms()
	{
		$this->load->view('admin/login.php');
	}
	public function adminpage()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('admin/index', $data);
	}
	public function cckind()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('admin/cc/inputkind', $data);
	}
	public function bank()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load->view('admin/cc/inputbank', $data);
	}
	public function inputcc()
	{
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['datatampilcombo']=$this->model_admin->getbank();
		$data['datatampilcombojeniskk']=$this->model_admin->getcardtype();
		$this->load->view('admin/cc/inputcc', $data);
	}
	public function member()
	{
		$this->load->view('admin/user/inputuser');
	}
	public function loginform()
	{
		$this->load->view('login');
	}
	public function daftarmember()
	{
		$this->load->view('daftar');
	}
	public function regist_member()
	{
		$this->load->view('regist');
	}
	public function profil($id)
	{
	    $data['noktp'] = $this->session->userdata('noktp');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['datatampilprofil']=$this->model_admin->tampilDatapribadi($id);
		$this->load->view('admin/profil', $data);
	}
	public function daftarcc()
	{
	    $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tampilData();
		$data['datatampilcombo']=$this->model_admin->getbank();
		$data['datatampilcombojeniskk']=$this->model_admin->getcardtype();
		$this->load->view('admin/cc/datacc', $data);
	}
	public function listmember()
	{
	    $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tampilDatamember();
		$this->load->view('admin/user/datauser', $data);
	}
	public function daftarbank()
	{
	    $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tampilDatabank();
		$this->load->view('admin/cc/databank', $data);
	}
	public function kindofcc()
	{
	    $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tampilDatajeniskk();
		$this->load->view('admin/cc/kindcc', $data);
	}
	public function daftarccsi()
	{
	    $data['noktp'] = $this->session->userdata('noktp');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tampilData();
		$data['datatampilcombo']=$this->model_admin->getbank();
		$data['datatampilcombojeniskk']=$this->model_admin->getcardtype();
		$this->load->view('admin/cc/succ_cc', $data);
	}
	public function daftarccfi()
	{
	    $data['noktp'] = $this->session->userdata('noktp');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tampilData();
		$data['datatampilcombo']=$this->model_admin->getbank();
		$data['datatampilcombojeniskk']=$this->model_admin->getcardtype();
		$this->load->view('admin/cc/failed_cc', $data);
	}
	public function insertcc()
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tambah();
		$this->load->view('member/cc/datacc', $data);
	}
	public function deletebank($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('model_admin','',TRUE); 
		$data['data']=$this->model_admin->hapus_bank($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/daftarbank');
            }
		$this->load->view('admin/cc/databank', $data);
	}
	public function deletekindcc($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('model_admin','',TRUE); 
		$data['data']=$this->model_admin->hapus_kindcc($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/kindofcc');
            }
		$this->load->view('admin/cc/kindcc', $data);
	}
	public function deletecc($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('model_admin','',TRUE); 
		$data['data']=$this->model_admin->hapus_d($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/daftarcc');
            }
		$this->load->view('admin/cc/datacc', $data);
	}
	public function deletemember($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('model_admin','',TRUE); 
		$data['data']=$this->model_admin->hapus_member($id);
		$data['data']=$this->model_admin->hapus_ccmember($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../admin/home/listmember');
            }
		$this->load->view('admin/user/datauser', $data);
	}
	
	
	public function edituser($id) { 
		
           $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->get_edituser($id);
		$this->load->view('admin/user/edituser', $data);

        } 
	public function editcc($id) { 
		
           $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['datatampilcombo']=$this->model_admin->getbank();
		$data['datatampilcombojeniskk']=$this->model_admin->getcardtype();
		$data['data']=$this->model_admin->get_edit($id);
		$this->load->view('admin/cc/editcc', $data);

        } 
	public function bankupdate($id) { 
		
           $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->get_editbank($id);
		$this->load->view('admin/cc/editbank', $data);

        } 
	public function editkindcc($id) { 
		
           $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->get_editkindcc($id);
		$this->load->view('admin/cc/editkind', $data);

        } 
		
	public function transcc($id) { 
		
           $data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['datatampil']=$this->model_admin->tampilDataperid($id);
		$data['datatampilwidgetlimit']=$this->model_admin->tampilDatawidgetlimit();
		$data['data']=$this->model_admin->get_edit($id);
		$this->load->view('member/cc/edit', $data);

        } 

        public function prosesedit_cc() { 
            $this->load->model('model_admin','',TRUE); 
            $this->model_admin->edit(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../admin/home/daftarcc');
        }
		public function prosesedit_member() { 
            $this->load->model('model_admin','',TRUE); 
            $this->model_admin->editmember(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../admin/home/listmember');
        }
		public function prosesedit_bank() { 
            $this->load->model('model_admin','',TRUE); 
            $this->model_admin->edit_bank(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../admin/home/daftarbank');
        }
		public function prosesedit_kind() { 
            $this->load->model('model_admin','',TRUE); 
            $this->model_admin->editkindcc(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            redirect('../admin/home/kindofcc');
        }
		public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('../home/admccms');
	}

	
	
}
