<?php
session_start();
class adminpage extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		}
		$this->load->helper('text');
		$this->load->helper(array('form', 'url'));
	}
	public function index() {

		//$data['daftar'] = $this->load->mod_blog->get_all();  
		$data['username'] = $this->session->userdata('username');
		
		
		$this->load->view('admin/index', $data,array('data' =>$data));
	

	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('./home/admccms');
	}

	     
}
