<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class input extends CI_Controller {

	
		 function add_kindcc()
		{
		
			$id_cardtype = $_POST['id_cardtype'];
			$cardtype = $_POST['cardtype'];
			
			$data = array(
			'id_cardtype' => $id_cardtype,
			'cardtype' => $cardtype
			);
			$res = $this->db->insert('t_jeniskk',$data);
			$this->load->library('form_validation');
		$this->form_validation->set_rules('id_cardtype','id_cardtype','required');
		$this->form_validation->set_rules('cardtype','cardtype','required');
		
		if($this->form_validation->run()==FALSE){
			$this->load->view('admin/cc/failed_kind');
		}else{
			$this->load->view('admin/cc/succ_kind');
		}
		}
		
		function add_bank()
		{
		
			$id_bank = $_POST['id_bank'];
			$nama_bank = $_POST['nama_bank'];
			$cabang = $_POST['cabang'];
			
			$data = array(
			'id_bank' => $id_bank,
			'nama_bank' => $nama_bank,
			'cabang' => $cabang
			);
			$res = $this->db->insert('t_bank',$data);
			$this->load->library('form_validation');
		$this->form_validation->set_rules('id_bank','id_bank','required');
		$this->form_validation->set_rules('nama_bank','nama_bank','required');
		$this->form_validation->set_rules('cabang','bank','required');
		
		if($this->form_validation->run()==FALSE){
			$this->load->view('admin/cc/failed_bank');
		}else{
			$this->load->view('admin/cc/succ_bank');
		}
		}
		
		public function add_cc()
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('model_admin');
		$data['data']=$this->model_admin->tambah();
		$this->load->view('member/cc/datacc', $data);
	}
}